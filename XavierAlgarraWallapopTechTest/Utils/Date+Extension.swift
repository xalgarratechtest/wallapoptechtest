//
//  DateUtils.swift
//  XavierAlgarraWallapopTechTest
//
//  Created by Xavier Algarra on 17/08/2020.
//  Copyright © 2020 Xavier Algarra. All rights reserved.
//

import Foundation

extension Date {
    init?(ISO8601: String) {
        guard let date = ISO8601DateFormatter().date(from: ISO8601) else {
            return nil
        }
        self = date
    }
}
