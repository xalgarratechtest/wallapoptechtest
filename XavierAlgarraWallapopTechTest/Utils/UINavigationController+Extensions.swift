//
//  UINavigationController+Extensions.swift
//  XavierAlgarraWallapopTechTest
//
//  Created by Xavier Algarra on 18/08/2020.
//  Copyright © 2020 Xavier Algarra. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    public var rootViewController : UIViewController? {
        return viewControllers.first
    }
}
