//
//  FileUtils.swift
//  XavierAlgarraWallapopTechTest
//
//  Created by Xavier Algarra on 15/08/2020.
//  Copyright © 2020 Xavier Algarra. All rights reserved.
//

import Foundation

struct FileUtils {
    static func data(fromFile file: String, ext: String) -> Data {
        guard let url = Bundle.main.url(forResource: file, withExtension: ext),
            let data = try? Data(contentsOf: url) else {
                return Data()
        }
        return data
    }
}
