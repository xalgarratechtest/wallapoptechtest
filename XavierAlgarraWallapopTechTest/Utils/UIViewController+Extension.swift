//
//  UIViewController+Extension.swift
//  XavierAlgarraWallapopTechTest
//
//  Created by Xavier Algarra on 17/08/2020.
//  Copyright © 2020 Xavier Algarra. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func createSpinnerView() -> SpinnerViewController{
        let spinner = SpinnerViewController()
        
        addChild(spinner)
        spinner.view.frame = view.frame
        view.addSubview(spinner.view)
        spinner.didMove(toParent: self)
        return spinner
    }
    
    func addspinner(spinner: SpinnerViewController) {
        addChild(spinner)
        spinner.view.frame = view.frame
        view.addSubview(spinner.view)
        spinner.didMove(toParent: self)
    }
    
    func removeSpinner(spinner: SpinnerViewController) {
        DispatchQueue.main.async {
            spinner.willMove(toParent: nil)
            spinner.view.removeFromSuperview()
            spinner.removeFromParent()
        }
    }
    
    func showError(error: String) {
        let AlertView = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        AlertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(AlertView, animated: true)
    }
}
