//
//  MarvelApi.swift
//  XavierAlgarraWallapopTechTest
//
//  Created by Xavier Algarra on 15/08/2020.
//  Copyright © 2020 Xavier Algarra. All rights reserved.
//

import Foundation
import Moya

enum MarvelApi {
    static private let pivKey = "2d302dce96f3794ffde915060d3f00b2c82bf801"
    static private let pubKey = "4d34ca58f2818f876cb9a3873ab3f21a"
    
    case heroes(page: Int, search: String?)
    case hero(heroId: String)
}

extension MarvelApi: TargetType {
    var baseURL: URL {
        guard let url = URL(string: "http://gateway.marvel.com/v1/public") else {
            return URL(string: "asda")!
        }
        return url
    }
    
    var path: String {
        switch self {
        case .hero(heroId: let heroId):
            return "/characters/\(heroId)"
        case .heroes:
            return "/characters"
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var sampleData: Data {
        switch self {
        case .heroes:
            return FileUtils.data(fromFile: "HerosAnswer", ext: "json")
        case .hero:
            return FileUtils.data(fromFile: "HerosAnswer", ext: "json")
        }
    }
    
    var task: Task {
        let timeStamp = "\(Date().timeIntervalSinceReferenceDate)"
        let hash = (timeStamp + MarvelApi.pivKey + MarvelApi.pubKey).MD5
        
        switch self {
        case .hero:
            return .requestParameters(parameters: ["apikey": MarvelApi.pubKey,
                                                   "ts": timeStamp,
                                                   "hash": hash],
                                      encoding: URLEncoding.default)
        case .heroes(page: let page, search: let search):
            var params = ["orderBy": "name",
                         "limit": 100,
                         "offset" : page,
                         "apikey": MarvelApi.pubKey,
                         "ts": timeStamp,
                         "hash": hash] as [String : Any]
            if search != nil {
                params["nameStartsWith"] = search
            }
            return .requestParameters(parameters: params,
                                      encoding: URLEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
}
