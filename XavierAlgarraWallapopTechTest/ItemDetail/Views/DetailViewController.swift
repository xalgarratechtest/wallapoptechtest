//
//  DetailViewController.swift
//  XavierAlgarraWallapopTechTest
//
//  Created by Xavier Algarra on 14/08/2020.
//  Copyright © 2020 Xavier Algarra. All rights reserved.
//

import UIKit
import SDWebImage

class DetailViewController: UIViewController {

    @IBOutlet weak var heroImage: SDAnimatedImageView!
    @IBOutlet weak var heroName: UILabel!
    @IBOutlet weak var heroNumber: UILabel!
    @IBOutlet weak var heroDescription: UITextView!
    var spinner: SpinnerViewController?
    
    var viewModel: ItemDetailViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setup()
    }
    
    func setup() {
        spinner = createSpinnerView()
        heroImage.image = UIImage(named: "PlaceholderSuperHero")?.imageWithGradient()
        viewModel?.delegate = self
        viewModel?.fetchData()
    }
}

extension DetailViewController: ItemDetailViewModelDelegate {
    func dataReadyToBeRead() {
        if let spinner = spinner {
            removeSpinner(spinner: spinner)
        }
        DispatchQueue.main.async {
            self.heroName.text = self.viewModel?.name
            self.heroDescription.text = self.viewModel?.description
            self.heroNumber.text = "\(self.viewModel?.numberOfComics ?? 0)"
            self.heroImage.sd_setImage(with: self.viewModel?.imageUrl, placeholderImage: UIImage(named: "PlaceholderSuperHero")) { (result, error, cache, url) in
                self.heroImage.image = result?.imageWithGradient()
            }
        }
    }
    
    func propagateError(error: String?) {
        showError(error: error ?? "unknown error")
    }
    
    
}
