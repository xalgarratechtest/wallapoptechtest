//
//  ItemDetailViewModel.swift
//  XavierAlgarraWallapopTechTest
//
//  Created by Xavier Algarra on 18/08/2020.
//  Copyright © 2020 Xavier Algarra. All rights reserved.
//

import Foundation
import Moya

protocol ItemDetailViewModelDelegate: class {
    func dataReadyToBeRead()
    func propagateError(error: String?)
}

class ItemDetailViewModel {
    
    private let heroId: String
    public weak var delegate: ItemDetailViewModelDelegate?
    
    private(set) var imageUrl: URL?
    private(set) var name: String?
    private(set) var numberOfComics: Int?
    private(set) var description: String?
    
    private let dataProvider: MoyaProvider<MarvelApi>
    
    
    init(heroId: String, dataProvider: MoyaProvider<MarvelApi> = MoyaProvider<MarvelApi>() ) {
        self.heroId = heroId
        self.dataProvider = dataProvider
    }
    
    func fetchData() {
        dataProvider.request(.hero(heroId: heroId)) { [weak self] result in
            guard let me = self else {
                return
            }
            print(result)
            switch result {
            case .success(let response):
                do {
                    let marvelRoot = try response.map(MarvelRoot.self)
                    
                    guard let hero = marvelRoot.data.results?.first else {
                        me.delegate?.propagateError(error: "unable to load the hero")
                        return
                    }
                    
                    me.name = hero.name
                    me.description = hero.heroDescription
                    let totalComics = hero.comics?.items?.count ?? 0
                    let totalStories = hero.stories?.items?.count ?? 0
                    let totalEvents = hero.events?.items?.count ?? 0
                        
                    me.numberOfComics = totalComics + totalStories + totalEvents
                    guard let path = hero.thumbnail?.completePath else {
                        me.imageUrl = nil
                        return
                    }
                    let url = URL(string: path)
                    me.imageUrl = url
                    me.delegate?.dataReadyToBeRead()
                } catch {
                    print(error)
                    me.delegate?.propagateError(error: error.localizedDescription)
                }
            case .failure( _):
                me.delegate?.propagateError(error: "Error loading data")
            }
        }
    }
}
