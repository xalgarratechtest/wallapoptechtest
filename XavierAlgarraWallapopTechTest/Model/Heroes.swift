//
//  Heroes.swift
//  XavierAlgarraWallapopTechTest
//
//  Created by Xavier Algarra on 15/08/2020.
//  Copyright © 2020 Xavier Algarra. All rights reserved.
//

import Foundation

// MARK: - MarvelAnswerRoot
struct MarvelRoot: Codable {
    let code: Int
    let status, copyright, attributionText, attributionHTML: String
    let etag: String
    let data: DataClass
}

// MARK: - DataClass
struct DataClass: Codable {
    let offset, limit, total, count: Int?
    let results: [Hero]?
}

// MARK: - Hero
struct Hero: Codable {
    let id: Int?
    let name, heroDescription: String?
    let untratedModified: String?
    let thumbnail: Thumbnail?
    let resourceURI: String?
    let comics, series: Comics?
    let stories: Stories?
    let events: Comics?
    let urls: [URLElement]?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case heroDescription = "description"
        case untratedModified = "modified"
        case thumbnail, resourceURI, comics, series, stories, events, urls
    }
    
    var modified: Date? {
        guard let date = untratedModified else {
            return nil
        }
        return Date(ISO8601: date)
    }
}

// MARK: - Comics
struct Comics: Codable {
    let available: Int?
    let collectionURI: String?
    let items: [Comic]?
    let returned: Int?
}

// MARK: - ComicsItem
struct Comic: Codable {
    let resourceURI: String?
    let name: String?
}

// MARK: - Stories
struct Stories: Codable {
    let available: Int?
    let collectionURI: String?
    let items: [Story]?
    let returned: Int?
}

// MARK: - StoriesItem
struct Story: Codable {
    let resourceURI: String?
    let name: String?
    let type: String?
}

// MARK: - Thumbnail
struct Thumbnail: Codable {
    let path: String?
    let thumbnailExtension: String?
    
    enum CodingKeys: String, CodingKey {
        case path
        case thumbnailExtension = "extension"
    }
}

extension Thumbnail {
    public var completePath: String? {
        guard let path = path, let thumbnailExtension = thumbnailExtension  else {
            return nil
        }
        return path + "." + thumbnailExtension
    }
}

// MARK: - URLElement
struct URLElement: Codable {
    let type: URLType?
    let url: String?
}

enum URLType: String, Codable {
    case comiclink = "comiclink"
    case detail = "detail"
    case wiki = "wiki"
}
