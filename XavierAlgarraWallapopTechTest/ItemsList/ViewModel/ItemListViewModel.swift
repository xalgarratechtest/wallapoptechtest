//
//  ItemListViewModel.swift
//  XavierAlgarraWallapopTechTest
//
//  Created by Xavier Algarra on 16/08/2020.
//  Copyright © 2020 Xavier Algarra. All rights reserved.
//

import Foundation
import Moya

protocol ItemListViewModelDelegate: class {
    /// Call when an error have to be shown
    /// - Parameter error: error description
    func propagateError(error: String)
    /// Call when an insert have been done in the results
    /// - Parameter indexPath: indexPath of the new element inserted
    func didInsertRow(indexPath: IndexPath)
    
    /// Call when an element have been modify in the results
    /// - Parameter indexPath: indexPath of the modified element
    func didUpdateRow(indexPath: IndexPath)
    
    /// Call when an element have been deleted from the results
    /// - Parameter indexPath: indexPath of the deleted element
    func didDeleteRow(indexPath: IndexPath)
    /// Notifies the receiver that there will be one or more updates due to an insert, remove,
    /// move, or update.
    func willChangeResults()
    
    func reloadData()
    
    /// Notifies the receiver that all updates have been completed. Results is updated
    func didChangeResults()
    func showLoading()
    func hideLoading()
}

class ItemListViewModel {
    
    private enum State {
        case loading
        case ready
    }
    
    private struct DataShown {
        var currentIndex: Int
        var nextIndex: Int
        var heroList: [Hero] = [Hero]()
        var search: String?
    }
    
    public struct Row {
        let name: String?
        let url: URL?
    }
    
    private var rows = [IndexPath:Row]()
    private let dataProvider: MoyaProvider<MarvelApi>
    private var state: State
    private var currentDataShown: DataShown
    private var runningRequest: Moya.Cancellable?
    
    /// Use in Delegate to serialize calls to VC to guarantee correct order.
    private let delegateHandlingQueue: OperationQueue = {
        let createe = OperationQueue()
        createe.name = "ItemList-DelegateHandlingQueue)"
        createe.qualityOfService = .userInteractive
        createe.maxConcurrentOperationCount = 1
        return createe
    }()
    
    public weak var delegate: ItemListViewModelDelegate?
    
    init(dataProvider: MoyaProvider<MarvelApi> = MoyaProvider<MarvelApi>()) {
        self.dataProvider = dataProvider
        state = .ready
        currentDataShown = DataShown(currentIndex: 0, nextIndex: 0)
    }
    
    public func fetchData(withSearch: String? = nil) {
        switch state {
        case .loading:
            if withSearch != nil {
                runningRequest?.cancel()
                fetchDataPrivate(withSearch: withSearch)
            }
            return
        case .ready:
            state = .loading
            fetchDataPrivate(withSearch: withSearch)
            return
        }
    }
    
    public func reloadData() {
        runningRequest?.cancel()
        delegate?.showLoading()
        state = .loading
        cleanData()
        cleanDataOperatio()
        fetchDataPrivate()
    }
    
    public func loadMore() {
        switch state {
        case .loading:
            return
        case .ready:
            currentDataShown.currentIndex += 1
            fetchDataPrivate()
        }
    }
    
    public func getRow(at: IndexPath) -> Row {
        if let row = rows[at] {
            return row
        }
        
        guard let url = currentDataShown.heroList[at.row].thumbnail?.completePath else {
            return Row(name: nil, url: nil)
        }
        let row = Row(name: currentDataShown.heroList[at.row].name, url: URL(string: url))
        rows[at] = row
        return row
    }
    
    public func numberOfRows() -> Int {
        return currentDataShown.heroList.count
    }
    
    public func itemDetailViewModel(indexPath: IndexPath) -> ItemDetailViewModel {
        return ItemDetailViewModel(heroId: String(currentDataShown.heroList[indexPath.row].id!))
    }
}

// MARK: - Private

extension ItemListViewModel {
    
    private func fetchDataPrivate(withSearch: String? = nil) {
        if currentDataShown.search != withSearch {
            cleanData()
            currentDataShown.search = withSearch
        }
        runningRequest = dataProvider.request(.heroes(page: currentDataShown.nextIndex, search: currentDataShown.search)) { [weak self] result in
            guard let me = self else {
                return
            }
            switch result {
            case .success(let response):
                do {
                    let heroes = try response.map(MarvelRoot.self)
                    if withSearch != nil {
                        me.cleanDataOperatio()
                    }
                    me.handlNewData(marvelRoot: heroes)
                } catch {
                    me.delegate?.propagateError(error: error.localizedDescription)
                }
            case .failure(let failure):
                if failure.errorCode != 6 {
                    me.delegate?.propagateError(error: "Error loading data")
                }
            }
            me.state = .ready
        }
    }
    
    private func handlNewData(marvelRoot: MarvelRoot) {
        guard let handlingIndex = marvelRoot.data.offset, let totalItems = marvelRoot.data.limit, let newHeroList = marvelRoot.data.results else {
            return
        }
        currentDataShown.currentIndex = handlingIndex
        currentDataShown.nextIndex = handlingIndex + totalItems
        dataWillBeSentOperation()
        newHeroList.forEach { (hero) in
            newHeroOperation(hero)
        }
        allDataSentOperation()
    }
    
    private func calculateNextIndexPath() -> IndexPath {
        let currentIndexPath = IndexPath(row: currentDataShown.heroList.count - 1, section: 0)
        return currentIndexPath
    }
    
    private func cleanData() {
        currentDataShown = DataShown(currentIndex: 0, nextIndex: 0)
        rows.removeAll()
    }
}

// MARK: - operations

extension ItemListViewModel {
    
    fileprivate func cleanDataOperatio () {
        self.delegateHandlingQueue.addOperation { [weak self] in
            guard let me = self else {
                // Valid case. We might have been dismissed.
                // Do nothing ...
                return
            }
            let group = DispatchGroup()
            group.enter()
            DispatchQueue.main.async {
                me.delegate?.showLoading()
                me.delegate?.reloadData()
                group.leave()
            }
            group.wait()
        }
    }
    
    fileprivate func newHeroOperation(_ hero: Hero) {
        self.delegateHandlingQueue.addOperation { [weak self] in
            guard let me = self else {
                // Valid case. We might have been dismissed.
                // Do nothing ...
                return
            }
            let group = DispatchGroup()
            group.enter()
            DispatchQueue.main.async {
                me.currentDataShown.heroList.append(hero)
                let indexPath = me.calculateNextIndexPath()
                me.delegate?.didInsertRow(indexPath: indexPath)
                group.leave()
            }
            group.wait()
        }
    }
    
    fileprivate func dataWillBeSentOperation() {
        delegateHandlingQueue.addOperation { [weak self] in
            guard let me = self else {
                // Valid case. We might have been dismissed.
                // Do nothing ...
                return
            }
            let group = DispatchGroup()
            group.enter()
            DispatchQueue.main.async {
                me.delegate?.hideLoading()
                me.delegate?.willChangeResults()
                group.leave()
            }
            group.wait()
        }
    }
    
    fileprivate func allDataSentOperation() {
        delegateHandlingQueue.addOperation { [weak self] in
            guard let me = self else {
                // Valid case. We might have been dismissed.
                // Do nothing ...
                return
            }
            let group = DispatchGroup()
            group.enter()
            DispatchQueue.main.async {
                me.delegate?.didChangeResults()
                group.leave()
            }
            group.wait()
        }
    }
}
