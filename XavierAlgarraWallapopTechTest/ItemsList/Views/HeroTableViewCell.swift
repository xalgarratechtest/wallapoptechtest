//
//  HeroTableViewCell.swift
//  XavierAlgarraWallapopTechTest
//
//  Created by Xavier Algarra on 17/08/2020.
//  Copyright © 2020 Xavier Algarra. All rights reserved.
//

import UIKit
import SDWebImage

class HeroTableViewCell: UITableViewCell {

    @IBOutlet weak var heroName: UILabel!
    
    func setBackground(url: URL) {
        let image = SDAnimatedImageView()
        backgroundView = image
        image.sd_imageIndicator = SDWebImageActivityIndicator.gray
        image.sd_setImage(with: url, placeholderImage: UIImage(named: "PlaceholderSuperHero")) { (result, error, cache, url) in
            image.image = result?.imageWithGradient()
        }
    }
}
