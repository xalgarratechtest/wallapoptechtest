//
//  MasterViewController.swift
//  XavierAlgarraWallapopTechTest
//
//  Created by Xavier Algarra on 14/08/2020.
//  Copyright © 2020 Xavier Algarra. All rights reserved.
//

import UIKit

class ItemListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var noDataFound: UIView!
    
    let refreshController = UIRefreshControl()
    var spinner: SpinnerViewController?
    var selectedIndexPath: IndexPath?
    let searchController = UISearchController()
    var lastSearchTerm: String?
    var somethingSearched: Bool = false
    
    var viewModel = ItemListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func loadData() {
        viewModel.fetchData()
    }
    
    @objc func resetData() {
        viewModel.reloadData()
    }
    
    func setup() {
        title = "Marvel Hero List"
        spinner = createSpinnerView()
        tableView.refreshControl = refreshController
        refreshController.addTarget(self, action: #selector(resetData), for: .valueChanged)
        viewModel.delegate = self
        navigationItem.searchController = searchController
        searchController.searchResultsUpdater = self
        searchController.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        self.navigationItem.hidesSearchBarWhenScrolling = false
        
        loadData()
    }
}

// MARK: - TableView Delegates

extension ItemListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.width
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "heroCell") as? HeroTableViewCell else {
            return UITableViewCell()
        }
        let row = viewModel.getRow(at: indexPath)
        cell.heroName.text = row.name
        cell.setBackground(url: row.url!)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if viewModel.numberOfRows() - 30 == indexPath.row {
            viewModel.loadMore()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        selectedIndexPath = indexPath
        performSegue(withIdentifier: "showHeroDetailSegue", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
}

// MARK: - Segues

extension ItemListViewController {
    // !!!: todo dependecy injection for detail view
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destination = segue.destination as? UINavigationController,
            let vc = destination.rootViewController as? DetailViewController,
            let indexPath = selectedIndexPath else {
                showError(error: "imposible to load DetailViewController")
                return
        }
        vc.viewModel = viewModel.itemDetailViewModel(indexPath: indexPath)
        
    }
    
}

extension ItemListViewController: UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchTerm = searchController.searchBar.text else {
            return
        }
        if searchTerm.count > 1 && lastSearchTerm != searchTerm {
            somethingSearched = true
            resetData()
            viewModel.fetchData(withSearch: searchTerm)
            lastSearchTerm = searchTerm
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        if somethingSearched {
            resetData()
        }
    }
}

extension ItemListViewController: ItemListViewModelDelegate {
    func reloadData() {
        self.tableView.reloadData()
    }
    
    func didInsertRow(indexPath: IndexPath) {
        self.tableView.insertRows(at: [indexPath], with: .automatic)
    }
    
    func didUpdateRow(indexPath: IndexPath) {
        self.tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    func didDeleteRow(indexPath: IndexPath) {
        self.tableView.deleteRows(at: [indexPath], with: .automatic)
    }
    
    func willChangeResults() {
        self.tableView.beginUpdates()
    }
    
    func didChangeResults() {
        self.tableView.endUpdates()
        if viewModel.numberOfRows() == 0 {
            tableView.backgroundView = noDataFound
        } else {
            tableView.backgroundView = UIView()
        }
    }
    
    func propagateError(error: String) {
        super.showError(error: error)
    }
    
    func showLoading() {
        if let spinner = spinner {
            addspinner(spinner: spinner)
        }
    }
    
    func hideLoading() {
        if let spinner = spinner {
            removeSpinner(spinner: spinner)
        }
    }
}
