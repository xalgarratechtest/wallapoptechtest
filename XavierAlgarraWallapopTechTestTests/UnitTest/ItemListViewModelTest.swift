//
//  ItemListViewModelTest.swift
//  XavierAlgarraWallapopTechTestTests
//
//  Created by Xavier Algarra on 16/08/2020.
//  Copyright © 2020 Xavier Algarra. All rights reserved.
//

import XCTest
import Moya
@testable import XavierAlgarraWallapopTechTest

class ItemListViewModelTest: XCTestCase {

    func testExample() throws {
        let provider = MoyaProvider<MarvelApi>(stubClosure: MoyaProvider.immediatelyStub)
        let insertExpectation = expectation(description: "insert")
        insertExpectation.expectedFulfillmentCount = 5
        let delegate = ItemListViewModelDelegateMock(expecteddidInsertRowCalled: insertExpectation, expectedwillChangeResultsCalled: expectation(description: "willChange"), expecteddidChangeResultsCalled: expectation(description: "didChange"), expectedhideLoadingCalled: expectation(description: "hideLoading"))
        let vm = ItemListViewModel(dataProvider: provider)
        vm.delegate = delegate
        vm.fetchData()
        
        waitForExpectations(timeout: 10)
        XCTAssertEqual(vm.numberOfRows(), 5)
    }
}

class ItemListViewModelDelegateMock: ItemListViewModelDelegate {
    
    var propagateErrorCalled: XCTestExpectation?
    var didInsertRowCalled: XCTestExpectation?
    var didUpdateRowCalled: XCTestExpectation?
    var didDeleteRowCalled: XCTestExpectation?
    var willChangeResultsCalled: XCTestExpectation?
    var reloadDataCalled: XCTestExpectation?
    var didChangeResultsCalled: XCTestExpectation?
    var showLoadingCalled: XCTestExpectation?
    var hideLoadingCalled: XCTestExpectation?
    
    init(expectedpropagateErrorCalled: XCTestExpectation? = nil,
         expecteddidInsertRowCalled: XCTestExpectation? = nil,
         expecteddidUpdateRowCalled: XCTestExpectation? = nil,
         expecteddidDeleteRowCalled: XCTestExpectation? = nil,
         expectedwillChangeResultsCalled: XCTestExpectation? = nil,
         expectedreloadDataCalled: XCTestExpectation? = nil,
         expecteddidChangeResultsCalled: XCTestExpectation? = nil,
         expectedshowLoadingCalled: XCTestExpectation? = nil,
         expectedhideLoadingCalled: XCTestExpectation? = nil) {
        self.propagateErrorCalled = expectedpropagateErrorCalled
        self.didInsertRowCalled = expecteddidInsertRowCalled
        self.didUpdateRowCalled = expecteddidUpdateRowCalled
        self.didDeleteRowCalled = expecteddidDeleteRowCalled
        self.willChangeResultsCalled = expectedwillChangeResultsCalled
        self.reloadDataCalled = expectedreloadDataCalled
        self.didChangeResultsCalled = expecteddidChangeResultsCalled
        self.showLoadingCalled = expectedshowLoadingCalled
        self.hideLoadingCalled = expectedhideLoadingCalled
        
    }
    
    func propagateError(error: String) {
        propagateErrorCalled?.fulfill()
    }
    
    func didInsertRow(indexPath: IndexPath) {
        didInsertRowCalled?.fulfill()
    }
    
    func didUpdateRow(indexPath: IndexPath) {
        didUpdateRowCalled?.fulfill()
    }
    
    func didDeleteRow(indexPath: IndexPath) {
        didDeleteRowCalled?.fulfill()
    }
    
    func willChangeResults() {
        willChangeResultsCalled?.fulfill()
    }
    
    func reloadData() {
        reloadDataCalled?.fulfill()
    }
    
    func didChangeResults() {
        didChangeResultsCalled?.fulfill()
    }
    
    func showLoading() {
        showLoadingCalled?.fulfill()
    }
    
    func hideLoading() {
        hideLoadingCalled?.fulfill()
    }
}
