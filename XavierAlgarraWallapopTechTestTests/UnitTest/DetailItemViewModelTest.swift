//
//  DetailItemViewModelTest.swift
//  XavierAlgarraWallapopTechTestTests
//
//  Created by Xavier Algarra on 18/08/2020.
//  Copyright © 2020 Xavier Algarra. All rights reserved.
//

import XCTest
import Moya
@testable import XavierAlgarraWallapopTechTest

class DetailItemViewModelTest: XCTestCase {
    
    var provider: MoyaProvider<MarvelApi>?
    
    override func setUp() {
        provider = MoyaProvider<MarvelApi>(stubClosure: MoyaProvider.immediatelyStub)
    }

    func testLoadData() throws {
        
        let viewModelDelegate = DetailItemViewModelDelegateMock(expectedDataReadyToBeReadCalled: true)
        guard let innerProvider = provider else {
            XCTFail()
            return
        }
        let viewModel = ItemDetailViewModel(heroId: "00000", dataProvider: innerProvider)
        viewModel.delegate = viewModelDelegate
        viewModel.fetchData()
        
        
        XCTAssertTrue(viewModelDelegate.dataReadyToBeReadCalled)
        XCTAssertFalse(viewModelDelegate.expectedpropagateErrorCalled)
        
        XCTAssertEqual("Moon Knight", viewModel.name)
        let url = URL(string: "http://i.annihil.us/u/prod/marvel/i/mg/3/30/52028af90e516.jpg")
        XCTAssertEqual(url, viewModel.imageUrl)
        XCTAssertEqual(46, viewModel.numberOfComics)
        XCTAssertEqual("", viewModel.description)
        
    }
}

class DetailItemViewModelDelegateMock: ItemDetailViewModelDelegate {
    var dataReadyToBeReadCalled = false
    var expectedDataReadyToBeReadCalled = false
    
    var propagateErrorCalled = false
    var expectedpropagateErrorCalled = false
    
    init(expectedDataReadyToBeReadCalled: Bool = false, expectedpropagateErrorCalled: Bool = false ) {
        self.expectedDataReadyToBeReadCalled = expectedDataReadyToBeReadCalled
        self.expectedpropagateErrorCalled = expectedpropagateErrorCalled
    }
    
    func dataReadyToBeRead() {
        if !expectedDataReadyToBeReadCalled {
            XCTFail()
        }
        dataReadyToBeReadCalled = true
    }
    
    func propagateError(error: String?) {
        if !expectedDataReadyToBeReadCalled {
            XCTFail()
        }
        propagateErrorCalled = true
    }
    
    
}
