//
//  ViewsSnapShotsTestViewsSnapShotsTest.swift
//  XavierAlgarraWallapopTechTestTests
//
//  Created by Xavier Algarra on 18/08/2020.
//  Copyright © 2020 Xavier Algarra. All rights reserved.
//

import XCTest
import FBSnapshotTestCase
import UIKit
import Moya
@testable import XavierAlgarraWallapopTechTest


class ViewsSnapShotsTestViewsSnapShotsTest: FBSnapshotTestCase {
    var waiter = XCTWaiter()
    var currentWindow: UIWindow {
        return UIApplication.shared.keyWindow!
    }
    var rootViewController: UIViewController? {
        get {
            return currentWindow.rootViewController
        }
        set(newRootViewController) {
            currentWindow.rootViewController = newRootViewController
            currentWindow.makeKeyAndVisible()
        }
    }
    
    func debugViewController(ViewController: UIViewController) {
        rootViewController = ViewController
        let exp = expectation(description: "No expectation")
        let _ = waiter.wait(for: [exp], timeout: 10)
        waiter = XCTWaiter()
    }
    
    
    
    override func setUp() {
        super.setUp()
        //self.recordMode = true
    }

    func testAppOpensAsExpected() throws {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let splitVc = storyboard.instantiateInitialViewController() as? UISplitViewController,
            let navVc = splitVc.viewControllers.first as? UINavigationController,
            let vc = navVc.rootViewController as? ItemListViewController else {
            XCTFail()
            return
        }
        FBSnapshotVerifyViewController(vc)
    }
    
    func testItemListShownAsExpected() throws {
        let itemListViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ItemListViewController") as! ItemListViewController
        itemListViewController.viewModel.delegate = nil
        itemListViewController.beginAppearanceTransition(true, animated: false)
        itemListViewController.endAppearanceTransition()
        
        let provider = MoyaProvider<MarvelApi>(stubClosure: MoyaProvider.immediatelyStub)
        let newViewModel = ItemListViewModel(dataProvider: provider)
        newViewModel.delegate = itemListViewController
        itemListViewController.viewModel = newViewModel
        newViewModel.reloadData()
        debugViewController(ViewController: itemListViewController)
        FBSnapshotVerifyViewController(itemListViewController)
    }
    
    func testDetailViewShownAsExpected() throws {
        let detailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ItemDetailViewController") as! DetailViewController
        detailViewController.viewModel = nil
        detailViewController.beginAppearanceTransition(true, animated: false)
        detailViewController.endAppearanceTransition()
        
        let provider = MoyaProvider<MarvelApi>(stubClosure: MoyaProvider.immediatelyStub)
        let newViewModel = ItemDetailViewModel(heroId: "1009452", dataProvider: provider)
        newViewModel.delegate = detailViewController
        detailViewController.viewModel = newViewModel
        newViewModel.fetchData()
        debugViewController(ViewController: detailViewController)
        FBSnapshotVerifyViewController(detailViewController)
        
    }


}
