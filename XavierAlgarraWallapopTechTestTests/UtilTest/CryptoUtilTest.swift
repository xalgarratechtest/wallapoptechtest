//
//  CryptoUtilTest.swift
//  XavierAlgarraWallapopTechTestTests
//
//  Created by Xavier Algarra on 15/08/2020.
//  Copyright © 2020 Xavier Algarra. All rights reserved.
//

import XCTest
import XavierAlgarraWallapopTechTest

class CryptoUtilTest: XCTestCase {

    func testStringMD5() throws {
        let toMD5 = "hello"
        // expected result of "hello" using http://www.md5.cz/ site.
        let expectedResult = "5d41402abc4b2a76b9719d911017c592"
        
        XCTAssertEqual(toMD5.MD5, expectedResult)
    }

}
